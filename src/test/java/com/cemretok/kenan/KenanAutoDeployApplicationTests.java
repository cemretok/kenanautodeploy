package com.cemretok.kenan;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cemretok.kenan.controller.ConfigController;
import com.cemretok.kenan.controller.GitController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class KenanAutoDeployApplicationTests {

	ConfigController configController;

	GitController gitController;

	@Before
	public void contextLoads() {
		configController = new ConfigController();
	}

	@Test
	public void is_config_file_load_properly() {
		assertThat(configController.configFileReader().size(), greaterThan(0));
	}

	@Test
	public void is_config_file_size_greater_than_0() {
		assertThat(configController.getRepositoryList().size(), greaterThan(0));
	}

	@Test
	public void is_key_value_match() {
		assertEquals("http://172.24.100.14:7990/scm/bc/sql_arbor.git",
				configController.getSpecificRepository("sql_arbor"));
	}

	@Test
	public void is_path_of_repository_equals_to_config() {
		assertEquals("/Users/cemretok/SQL_CODE", configController.getBitbucketSpecificKey("repository_path"));
	}
}
