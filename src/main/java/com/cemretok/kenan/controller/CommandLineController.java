package com.cemretok.kenan.controller;

import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CommandLineController {

	private GitController gitController;
	private DeploymentController deploymentController;

	private static final Logger log = LoggerFactory.getLogger(CommandLineController.class);

	public CommandLineController(GitController gitController, DeploymentController deploymentController) {
		this.gitController = gitController;
		this.deploymentController = deploymentController;
	}

	public void autoDeploymentController(String[] args) {
		Scanner scanner;
		boolean isArgsExists = args.length == 1;
		if (isArgsExists) {
			switch (args[0]) {
			case "clone":
				log.info("Repository Cloning in progress.");
				gitController.cloneAllRepositories();
				log.info("Repository Cloning finished.");
				break;
			case "refresh":
				log.info("Refresh in progress.");
				gitController.refreshAllRepositories();
				log.info("Refresh finished.");
				break;
			}
		} else {
			for (;;) {
				// TODO: Stringler final olarak bir sınıfta tutulacak.
				System.out.println("1. Clone All Repositories in config with 'clone' command.");
				System.out.println("2. Refresh All Repositories in config with 'refresh' command.");
				System.out.println("3. Prepare deployable files with 'prepare DEF-12345' command.");
				System.out.println("4. Move files with 'move DEF-12345 prod' command.");
				System.out.println("5. Deploy prepared files with 'deploy prod' command.");
				System.out.println("6. Exit from program with 'exit' command.");

				scanner = new Scanner(System.in);

				String keyboardInput = scanner.nextLine();

				boolean isValidCommand = keyboardInput.contains(" ") || keyboardInput.split(" ").length <= 3;

				if (isValidCommand) {
					String[] commandArray = keyboardInput.split(" ");

					String command = commandArray.length <= 3 ? commandArray[0] : "Default";
					switch (command) {
					case "clone":
						log.info("Repository Cloning in progress.");
						gitController.cloneAllRepositories();
						log.info("Repository Cloning finished.");
						break;
					case "refresh":
						log.info("Refresh in progress.");
						gitController.refreshAllRepositories();
						log.info("Refresh finished.");
						break;
					case "prepare":
						log.info("File preperation in progress.");
						gitController.refreshAllRepositories();
						deploymentController.prepareDeploymentFile(commandArray[1]);
						log.info("File preperation finished.");
						break;
					case "move":
						log.info("File movement in progress.");
						deploymentController.movePreparedDeploymentFile(commandArray[1], commandArray[2]);
						log.info("File movement file finished.");
						break;
					case "deploy":
						log.info("File deployment in progress.");
						deploymentController.installPreparedDeploymentFile(commandArray[1]);
						log.info("File deployment finished.");
						break;

					default:
						System.out.println("Parametre Kontrolü yapılmalı.");
					}
				}
			}
		}
	}
}
