package com.cemretok.kenan.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class DeploymentController {
	private static final Logger log = LoggerFactory.getLogger(DeploymentController.class);

	private ConfigController configController;
	private GitController gitController;
	private FTPController ftpController;
	private InstallerController installerController;

	public DeploymentController(ConfigController configController, GitController gitController,
			FTPController ftpController, InstallerController installerController) {
		this.configController = configController;
		this.gitController = gitController;
		this.ftpController = ftpController;
		this.installerController = installerController;
	}

	public List<String> deploymentFileReader(String deploymentFileName) {
		List<String> deploymentFile = null;
		try (Stream<String> lines = Files.lines(Paths
				.get(configController.getBitbucketSpecificKey("deployment_path").concat("/").concat(deploymentFileName)
						.concat("/").concat(configController.getBitbucketSpecificKey("deployment_cfg"))))) {
			deploymentFile = lines.collect(Collectors.toList());
		} catch (IOException e) {
			e.getStackTrace();
			log.error("Can't read ".concat(deploymentFileName));
		}

		return deploymentFile;
	}

	public void prepareDeploymentFile(String deploymentFileName) {
		String[] splittedDeploymentConfig = null;

		for (String deploymentLine : deploymentFileReader(deploymentFileName)) {
			splittedDeploymentConfig = deploymentLine.split(",");
			// TODO: Eğer dosya zaten varsa önce bunu silinmesi sağlanacak.
			// TODO: Eğer aranan dosyayı dizini yada başka birşeyden dolayı hata alırsa bu
			// hata exception ile düzeltilecek.
			gitController.gitRepositoryCodeFinder(configController.getBitbucketSpecificKey("repository_path"),
					splittedDeploymentConfig[1], splittedDeploymentConfig[2], splittedDeploymentConfig[3],
					configController.getBitbucketSpecificKey("deployment_path").concat("/")
							.concat(splittedDeploymentConfig[0]).concat("/").concat(splittedDeploymentConfig[3]));
		}
	}

	public void movePreparedDeploymentFile(String deploymentFileName, String deploymentServer) {
		String[] splittedDeploymentConfig = null;

		for (String deploymentLine : deploymentFileReader(deploymentFileName)) {
			splittedDeploymentConfig = deploymentLine.split(",");
			// TODO: Eğer dosya zaten varsa önce bunu silinmesi sağlanacak.
			// TODO: Eğer aranan dosyayı dizini yada başka birşeyden dolayı hata alırsa bu
			// hata exception ile düzeltilecek.

			try {
				File deploymentFile = new File(configController.getBitbucketSpecificKey("deployment_path").concat("/")
						.concat(splittedDeploymentConfig[0]).concat("/").concat(splittedDeploymentConfig[3]));
				ftpController.putFileToPath(configController.getServerSpecificKey(deploymentServer, "code_path"),
						deploymentFile, deploymentServer);
				log.info(splittedDeploymentConfig[3].concat(" has been deployed."));
				ftpController.closeFTPConnection();
			} catch (IOException e) {
				e.printStackTrace();
				log.error("Can't move files");
			}
		}
	}

	public void installPreparedDeploymentFile(String deploymentEnvironment) {
		installerController.executeRemoteSSHCommand(deploymentEnvironment);
	}

}
