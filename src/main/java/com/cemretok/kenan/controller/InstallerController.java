package com.cemretok.kenan.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

@Service
public class InstallerController {
	private static final Logger log = LoggerFactory.getLogger(InstallerController.class);

	private String remoteUser;
	private String remotePassword;
	private String remoteIp;
	private int remotePort;
	private String cfgPath;
	private String codePath;
	private String installerPath;
	private String command;

	private ConfigController configController;

	public InstallerController(ConfigController configController) {
		this.configController = configController;
	}

	public void getRemoteConnectionConfiguration(String environment) {
		remoteUser = configController.getServerSpecificKey(environment, "user");
		remotePassword = configController.getServerSpecificKey(environment, "password");
		remoteIp = configController.getServerSpecificKey(environment, "ip");
		remotePort = Integer.parseInt(configController.getServerSpecificKey(environment, "port"));
		cfgPath = configController.getServerSpecificKey(environment, "cfg_path");
		codePath = configController.getServerSpecificKey(environment, "code_path");
		installerPath = configController.getServerSpecificKey(environment, "installer_path");
		command = configController.getServerSpecificKey(environment, "command");
	}

	public Session openRemoteSSHConnection(String environment) {
		getRemoteConnectionConfiguration(environment);
		log.info("3");
		JSch jsch = new JSch();
		Session session = null;
		System.out.println(remoteUser + remoteIp + remotePort);
		try {
			session = jsch.getSession(remoteUser, remoteIp, remotePort);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(remotePassword);
			session.connect();
		} catch (JSchException e) {
			log.error("Can't open " + environment + " SSH connection.");
			e.printStackTrace();
		}
		log.info("4");
		return session;
	}

	public void closeRemoteSSHConnection(ChannelExec channelExec, Session session) {
		try {
			channelExec.disconnect();
			session.disconnect();
		} catch (Exception e) {
			log.error("Can't disconnect from SSH.");
			e.printStackTrace();
		}
	}

	public void executeRemoteSSHCommand(String environment) {
		Session session = openRemoteSSHConnection(environment);
		ChannelExec channelExec = null;
		try {
			channelExec = (ChannelExec) session.openChannel("exec");
			InputStream in = channelExec.getInputStream();

			channelExec.setCommand("sh ".concat(installerPath.concat("/").concat(command)));

			// channelExec.setCommand(command);

			channelExec.connect();

			BufferedReader reader = new BufferedReader(new InputStreamReader(in));

			String line;

			while ((line = reader.readLine()) != null) {
				System.out.println(line);
				log.info("2");
			}

			// int exitStatus = channelExec.getExitStatus();

			closeRemoteSSHConnection(channelExec, session);

			log.info("SSH Completed.");
		} catch (JSchException e) {
			log.error("SSH command error.");
			e.printStackTrace();
		} catch (IOException e) {
			log.error("Input write error.");
			e.printStackTrace();
		} finally {
			closeRemoteSSHConnection(channelExec, session);
		}
	}

}
