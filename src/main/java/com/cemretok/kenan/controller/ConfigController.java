package com.cemretok.kenan.controller;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

@Service
public class ConfigController {

	public ConfigController() {

	}

	public JSONObject configFileReader() {
		JSONParser parser = new JSONParser();

		JSONObject jsonConfig = null;
		try {
			jsonConfig = (JSONObject) parser
					.parse(new FileReader(System.getProperty("user.dir").concat("/config/config.json")));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonConfig;
	}

	public Map<String, String> getRepositoryList() {
		JSONObject bitbucketObject = (JSONObject) configFileReader().get("bitbucket");

		@SuppressWarnings("unchecked")
		Map<String, String> repositoryObject = (JSONObject) bitbucketObject.get("repositories");
		System.out.println(repositoryObject);
		return repositoryObject;
	}

	public String getBitbucketSpecificKey(String key) {
		JSONObject bitbucketObject = (JSONObject) configFileReader().get("bitbucket");

		return (String) bitbucketObject.get(key);
	}

	public String getServerSpecificKey(String environment, String key) {
		JSONObject serverObject = (JSONObject) configFileReader().get("server");
		JSONObject environmentObject = (JSONObject) serverObject.get(environment);
		return (String) environmentObject.get(key);
	}

	public String getSpecificRepository(String key) {
		return (String) getRepositoryList().get(key);
	}

}
