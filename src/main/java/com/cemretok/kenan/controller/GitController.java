package com.cemretok.kenan.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.ResetCommand.ResetType;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.RevisionSyntaxException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class GitController {
	private static final Logger log = LoggerFactory.getLogger(GitController.class);

	Repository repository = null;
	Git git = null;

	private ConfigController configController;

	public GitController(ConfigController configController) {
		this.configController = configController;
	}

	public void cloneAllRepositories() {
		String repositoryPath = configController.getBitbucketSpecificKey("repository_path");
		String projectLink = configController.getBitbucketSpecificKey("project_link");
		configController.getRepositoryList()
				.forEach((keyName, valueLink) -> cloneRepository(keyName, projectLink.concat(valueLink), repositoryPath));
	}

	public void cloneRepository(String repositoryName, String repositoryLink, String repositoryPath) {
		if (!checkRepositoryFileisExistsinDirectory(repositoryPath, repositoryName)) {
			try {
				Git.cloneRepository().setURI(repositoryLink).setCredentialsProvider(getRepositoryCridential())
						.setDirectory(new File(repositoryPath + "/" + repositoryName))
						.setBranchesToClone(Arrays.asList("refs/heads/am-development")).setBranch("am-development")
						.call();
			} catch (GitAPIException e) {
				log.info(repositoryName + " has not development branch!");
				log.info("Git error: " + e);
			}
		}
	}

	public void refreshAllRepositories() {
		configController.getRepositoryList().forEach((keyName, valueLink) -> refreshRepository(keyName,
				configController.getBitbucketSpecificKey("repository_path")));
	}

	public void refreshRepository(String repositoryName, String repositoryPath) {
		if (checkRepositoryFileisExistsinDirectory(repositoryPath, repositoryName)) {
			Git git = null;
			PullCommand pullCommand;

			git = new Git(getRepositoryInDirectory(repositoryPath, repositoryName));

			try {
				pullCommand = git.pull();
				git.reset().setMode(ResetType.HARD).setRef("refs/heads/am-development").call();
				pullCommand.setRemoteBranchName("refs/heads/am-development").setRebase(true)
						.setCredentialsProvider(getRepositoryCridential()).call();
			} catch (GitAPIException e) {
				e.printStackTrace();
			}
			git.close();
		}
	}

	public Repository checkoutRepository(String repositoryPath, String repositoryName, String checkoutId) {
		try {
			repository = getRepositoryInDirectory(repositoryPath, repositoryName);
			git = new Git(repository);

			git.checkout().setName(checkoutId == null ? "development" : checkoutId).call();
		} catch (GitAPIException e) {
			repository.close();
			git.close();
			log.error("Given commit id (" + checkoutId + ") is not belong " + repositoryName + " repository.");
		}

		return repository;
	}

	public Repository getRepositoryInDirectory(String repositoryPath, String repositoryName) {
		try {
			return new FileRepository(repositoryPath + "/" + repositoryName + "/.git");
		} catch (IOException e) {
			log.error("Given repository (" + repositoryName + ") is not exists");
		}
		return repository;
	}

	public UsernamePasswordCredentialsProvider getRepositoryCridential() {
		return new UsernamePasswordCredentialsProvider(configController.getBitbucketSpecificKey("user"),
				configController.getBitbucketSpecificKey("password"));
	}

	public void gitRepositoryCodeFinder(String repositoryPath, String repositoryName, String checkoutId,
			String fileName, String deploymentPath) {

		Repository repository = getRepositoryInDirectory(repositoryPath, repositoryName);
		ObjectId lastCommitId = null;
		boolean isFileFound = false;

		try {
			lastCommitId = repository.resolve(checkoutId);
		} catch (RevisionSyntaxException | IOException e1) {
			e1.printStackTrace();
		}
		try (RevWalk revWalk = new RevWalk(repository)) {
			RevCommit commit = revWalk.parseCommit(lastCommitId);
			System.out.println(commit.getFullMessage());
			RevTree tree = commit.getTree();
			try (TreeWalk treeWalk = new TreeWalk(repository)) {
				treeWalk.addTree(tree);
				treeWalk.setRecursive(true);
				while (treeWalk.next()) {
					if (treeWalk.getNameString().equals(fileName)) {
						ObjectId objectId = treeWalk.getObjectId(0);
						ObjectLoader loader = repository.open(objectId);

						FileOutputStream fos = new FileOutputStream(deploymentPath, true);
						fos.write(loader.getBytes());
						fos.close();

						isFileFound = true;
					}
				}
			}
			revWalk.dispose();

			if (!isFileFound)
				log.error("Did not find expected file " + fileName);

		} catch (IncorrectObjectTypeException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean checkRepositoryFileisExistsinDirectory(String repositoryPath, String repositoryName) {
		File repositoryFile = new File(repositoryPath + "/" + repositoryName);

		return repositoryFile.exists();
	}
}
