package com.cemretok.kenan.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

@Service
public class FTPController {
	Session session = null;
	Channel channel = null;
	ChannelSftp channelSftp = null;

	private ConfigController configController;

	private static final Logger log = LoggerFactory.getLogger(FTPController.class);

	public FTPController(ConfigController configController) {
		this.configController = configController;
	}

	public ChannelSftp openFTPConnection(String environment) throws IOException {
		JSch jsch = new JSch();
		try {
			session = jsch.getSession(configController.getServerSpecificKey(environment, "user"),
					configController.getServerSpecificKey(environment, "ip"),
					Integer.parseInt(configController.getServerSpecificKey(environment, "port")));

			session.setPassword(configController.getServerSpecificKey(environment, "password"));
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			System.out.println("Host connected.");
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;

			System.out.println("sftp channel opened and connected.");
		} catch (NumberFormatException | JSchException e) {
			log.error("SCP connection getting error");
			e.printStackTrace();
		}

		return channelSftp;
	}

	public void closeFTPConnection() throws IOException {
		channelSftp.disconnect();
		session.disconnect();
	}

	public void putFileToPath(String path, File file, String deploymentServer) throws IOException {
		ChannelSftp channelSftp = openFTPConnection(deploymentServer);

		try {
			channelSftp.cd(path);
			channelSftp.put(new FileInputStream(file), file.getName());

			log.info("File transfered successfully to host.");
		} catch (SftpException e) {
			e.printStackTrace();
			log.error("File transfer gets error.");
		}

	}
}
