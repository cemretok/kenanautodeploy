package com.cemretok.kenan;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.cemretok.kenan.controller.CommandLineController;

@SpringBootApplication
public class KenanAutoDeployApplication implements CommandLineRunner {

	private CommandLineController commandLineController;

	public KenanAutoDeployApplication(CommandLineController commandLineController) {
		this.commandLineController = commandLineController;
	}

	public static void main(String[] args) {
		SpringApplication.run(KenanAutoDeployApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		commandLineController.autoDeploymentController(args);
	}

}
