package com.cemretok.kenan;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.cemretok.kenan" })
@EntityScan(basePackages = { "com.cemretok.kenan" })
public class KenanAutoDeployConfiguration {

}
